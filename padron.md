# Padrón

## Indicadores

### SB10

- Habitantes
- País


### SB21

- Habitantes
- Municipio

### SB28

- Habitantes
- Fecha

### SB29

- Habitantes
- Sexo

### SB30

- Habitantes
- Edad
- Fecha

### SB32

- Habitantes
- Edad
- Fecha

### SB33

- Municipio
- Hogares Censados

### SB35

- Hogares Censados
- Nº Personas viviendo en el hogar

### SB36

- Hogares Censados
- Nº Personas viviendo en el hogar
- Edad

### SB8

- Habitantes
- Edad

### SB9

- Habitantes
- Edad

# Entidades

## Entidad datos padrón

**Indicadores:** SB10, SB21, SB28, SB29, SB30, SB32, SB8, SB9

- Habitantes
- País
- provincia
- Fecha
- Edad
- Sexo

### Dimensión geográfica

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_geografica` (
	pais varchar,
	provincia varchar,
);
```

### Dimensión personal

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_personal` (
	edad int,
	sexo varchar,
);
```


### Dimensión fecha

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_fecha` (
	fecha date,
);
```

### Resultado en tabla de entidad datos padrón

- ID [Revisar]
- Dimensión geográfica
- Dimensión personal
- Dimensión fecha
- Habitantes

### JSON de la entidad datos padrón

```
{
	"id": { 
		"type": "string", 
	    "required": true,
	    unique": true 
	},
	"type": { 
	    "type": "string", 
	    "required": true, 
	    "default": "<tipo-de-entidad>"
	},
	"habitantes": {
		"value": "<n-habitantes>",
		"type": "Number"
	},
	"pais": {
		"value": <nombre-pais>,
		"type": "Text"
	},
	"provincia": {
		"value": <nombre-provincia>,
		"type": "Text"
	},
	"fecha": {
		"value": <fechao>,
		"type": "Date"
	},
	"edad": {
		"value": <edad>,
		"type": "Number"
	},
	"sexo": {
		"value": <sexo>,
		"type": "Text"
	}
}
```

## Entidad hogares censados

**Indicadores:** SB33, SB35, SB36

- Hogares censados
- Nº Personas viviendo en el hogar
- Edad

### Dimensión unipersonal

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_unipersonal` (
	numeroPersonas int,
	edad int,
);
```

### Resultado en tabla de entidad hogares censados

- ID [Revisar]
- Dimensión unipersonal
- Hogares censados
- Dimensión edad

### JSON de la entidad hogares censados

```
{
	"id": { 
		"type": "string", 
	    "required": true,
	    unique": true 
	},
	"type": { 
	    "type": "string", 
	    "required": true, 
	    "default": "<tipo-de-entidad>"
	},
	"value": <n-hogares-censados>,
	"type": "Number"
	},
	"n-personas-viviendo": {
		"value": <n-personas-viviendo>,
		"type": "Number"
	},
	"edad": {
		"value": <edad>,
		"type": "Number"
	}
}
```