# Turismo

## Indicadores

### E12

- Fecha
- Edad
- País

### E21

- Fecha
- TipoDía
- País

### E22

- Fecha
- TipoDía
- País

### E5 

- Fecha
- Provincia

### E6

- Fecha
- Provincia

### E7

- Fecha
- País

# Entidades

## Entidad turismo

**Indicadores:** E12, E21, E22, E5, E6, E7

- ID
- fechaInicio
- fechaFin
- tipoDia
- edad
- pais
- provincia
- Visitas [Medida]

### Dimensión fecha

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_fecha` (
	fechaInicio date,
	fechaFin date,
	tipoDia varchar,
);
```

### Dimensión edad

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_edad` (
	edad int,
);
```

### Dimensión geográfica

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_geografica` (
	pais varchar,
	provincia varchar,
);
```

### Resultado en tabla

- ID [Agrupar todas las columnas o generar un identificador]
- Dimensión Fecha
- Dimensión Edad
- Dimensión Geográfica
- Visitas [Medida]

### JSON de la entidad

```
{
	"id": { 
		"type": "string", 
	    "required": true,
	    unique": true 
	},
	"type": { 
	    "type": "string", 
	    "required": true, 
	    "default": "<tipo-de-entidad>"
	},
	"fechaInicio": {
		"value": "<fecha-inicio>",
		"type": "date"
	},
	"fechaFin": {
		"value": "<fecha-final>",
		"type": "date"
	},
	"tipoDia": {
		"value": "[laborable|festivo]",
		"type": "Text"
	},
		"edad": {
		"value": "<edad>",
		"type": "Number"
	},
	"pais": {
		"value": "<nombre-pais>",
		"type": "Text"
	},
		"provincia": {
		"value": "<nombre-provincia>",
		"type": "Text"
	}
}
```