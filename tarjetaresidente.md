# Tarjeta Residente

## Indicadores

### G36

- Estado tarjeta
- Fecha

### G37

- Estado tarjeta
- Fecha realización
- Nº Tarjeta

### G38

- Fecha realización
- Importe descontado

### G39

- Fecha realización
- Importe descontado

### G40

- Fecha realización
- Importe recarga

### G41

- Nº Tarjeta
- Ámbito
- Centro
- Estado tarjeta


# Entidades

## Entidad estados de tarjeta

**Indicadores:** G36, G37

- ID
- Estado Tarjeta
- Fecha de Realización
- Nº de Tarjeta

### Dimensión estado tarjeta

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_estadoTarjeta` (
	estadoTarjeta varchar,
);
```

### Dimensión fecha realización

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_fechaRealizacion` (
	fechaRealizacion date,
);
```

### Resultado en tabla de la entidad estados de tarjeta

- ID [Revisar]
- Dimensión estadoTarjeta
- Dimensión fechaRealización
- Nº de Tarjeta

### JSON de la entidad estados de tarjeta

```
{
	"id": { 
		"type": "string", 
	    "required": true,
	    unique": true 
	},
	"type": { 
	    "type": "string", 
	    "required": true, 
	    "default": "<tipo-de-entidad>"
	},
	"estadoTarjeta": {
		"value": "<estado-tarjeta>",
		"type": "Text"
	},
	"fechaRealización": {
		"value": <fecha-realización>,
		"type": "Date"
	},
	"numeroTarjeta": {
		"value": <numero-tarjeta>,
		"type": "Number"
	}
}
```

## Entidad importes de tarjeta

**Indicadores:** G38, G39, G40

- ID
- Fecha de Realización
- Importe descontado
- Importe recargado

### Dimensión tipoImporte
```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_importe` (
	importeDescontado varchar,
	importeRecargado varchar,
);
```

### Dimensión fecha realización

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_fechaRealizacion` (
	fechaRealizacion date,
);
```

### Resultado en tabla de la entidad importes de tarjeta

- ID [Revisar]
- Dimensión fechaRealización
- Importe
- Dimensión tipoImporte

### JSON de la entidad importes

```
{
	"id": { 
		"type": "string", 
	    "required": true,
	    unique": true 
	},
	"type": { 
	    "type": "string", 
	    "required": true, 
	    "default": "<tipo-de-entidad>"
	},
	"fechaRealización": {
		"value": <fecha-realización>,
		"type": "Date"
	},
	"importeDescontado": {
		"value": <importe-descontado>,
		"type": "Number"
	},
	"importeRecargado": {
		"value": <importe-recargado>,
		"type": "Number"
	}
}
```

## Entidad servicios utilizados

**Indicadores:** G41

- ID
- Estado tarjeta
- Ámbito
- Centro

### Dimensión servicio

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_servicio` (
	ambito varchar,
	centro varchar,
);
```

### Dimensión estado tarjeta

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_estadoTarjeta` (
	estadoTarjeta varchar,
);
```

### Resultado en tabla de la entidad servicios utilizados

- ID [Revisar]
- Dimensión estadoTarjeta
- Dimensión servicio

### JSON de la entidad servicios

```
{
	"id": { 
		"type": "string", 
	    "required": true,
	    unique": true 
	},
	"type": { 
	    "type": "string", 
	    "required": true, 
	    "default": "<tipo-de-entidad>"
	},
	"ambito": {
		"value": <ambito>,
		"type": "Text"
	},
	"centro": {
		"value": <centro>,
		"type": "Text"
	},
	"estadoTarjeta": {
		"value": "<estado-tarjeta>",
		"type": "Text"
	}
}
```