# Modelo fuente

## TablasDetalle

- Intervalo fechas
- Tipo de dia (laborable / festivo)
- Tramo horario
- Genero
- Rango edades
- Provincia
- Pais
- Numero visitantes

## Tablas por tiempo de estancia

- Intervalo fecha
- Provincia
- Pais
- Numero de visitantes
- Estancia media

# Modelo entidad

## Indicadores

### E5

Porcentaje de visitantes que proceden de la provincia de Badajoz

Atributo    | Tipo      | Tabla detalle  | Tabla estancia
----------- | --------- | -------------- | ---------------
intervalo   | dimension | X              | X
provincia   | dimension | X              | X
pais        | dimension | X              | X
visitas     | medida    | X              | X

### E6

Porcentaje de visitantes que proceden del resto de provincias Españolas

Atributo    | Tipo      | Tabla detalle  | Tabla estancia
----------- | --------- | -------------- | ---------------
intervalo   | dimension | X              | X
provincia   | dimension | X              | X
pais        | dimension | X              | X
visitas     | medida    | X              | X

### E7

Porcentaje de visitantes que proceden de otros países

Atributo    | Tipo      | Tabla detalle  | Tabla estancia
----------- | --------- | -------------- | ---------------
intervalo   | dimension | X              | X
provincia   | dimension | X              | X
pais        | dimension | X              | X
visitas     | medida    | X              | X

### E12

Porcentaje de los turistas nacionales que tienen 65 o más años de edad

Atributo    | Tipo      | Tabla detalle  | Tabla estancia
----------- | --------- | -------------- | ---------------
intervalo   | dimension | X              | X
provincia   | dimension | X              | X
pais        | dimension | X              | X
rango_edad  | dimension | X              | -
visitas     | medida    | X              | X

### E21

Para el periodo considerado, día de la semana que presenta el mayor número de visitantes de nacionalidad española

Atributo    | Tipo      | Tabla detalle  | Tabla estancia
----------- | --------- | -------------- | ---------------
intervalo   | dimension | X              | X
provincia   | dimension | X              | X
pais        | dimension | X              | X
dia         | dimension | -              | -
visitas     | medida    | X              | X

### E22

Para el periodo considerado, día de la semana que presenta el mayor número de visitantes de nacionalidad no española

Atributo    | Tipo      | Tabla detalle  | Tabla estancia
----------- | --------- | -------------- | ---------------
intervalo   | dimension | X              | X
provincia   | dimension | X              | X
pais        | dimension | X              | X
dia         | dimension | -              | -
visitas     | medida    | X              | X

### E23

Para el periodo considerado, porcentaje que representan los visitantes habituales sobre el número medio de visitantes (Consideramos visitantes habituales aquellos que visitan la ciudad al menos tres días en un mes, no siendo nunca dos días consecutivos entre si)

Atributo       | Tipo      | Tabla detalle  | Tabla estancia
-----------    | --------- | -------------- | ---------------
intervalo      | dimension | X              | X
provincia      | dimension | X              | X
pais           | dimension | X              | X
visitas        | medida    | X              | X
duracion_media | medida    | -              | -

//TODO: Hacer este mismo análisis para opendata

# Atributos del modelo BDA

## Dimensiones

### Dimension temporal

- periodo
- dia de la semana

```
#!sql
CREATE TABLE IF NOT EXISTS `dimension_temporal` (

)
```

### Dimension geográfica

- Provincia
- Pais

## Dimension  demográfica

- Rango de edad

## Medidas

- Numero de visitantes
- Duracion media de la estancia

# Deficiencias:

- Necesitamos información del día de la semana, según el KPI E22. Sin embargo, las fuente de datos solo distingue entre laborables y festivos.
- Necesitamos información de visitantes habituales según KPI E23. La fuente de datos no contiene información de número medio de visitas, sólo de estancia media.
- No tenemos medios para hallar la edad media, lo que se tiene son rangos de edades.
